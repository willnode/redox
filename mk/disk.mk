$(BUILD)/harddrive.img: $(FSTOOLS_TAG) $(REPO_TAG)
	-cat $(ROOT)/.config > $(ROOT)/builder/.config 
	podman build -t redox-builder -f $(ROOT)/builder/Dockerfile
	podman run --rm --sig-proxy \
		-v $(ROOT)/config:/config \
		-v $(ROOT)/cookbook/build:/cookbook/build \
		-v $(ROOT)/cookbook/repo:/cookbook/repo \
		-v $(ROOT)/$(BUILD):/build --privileged redox-builder
