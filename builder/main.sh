
export $(cat /.config | xargs)


export FILESYSTEM_CONFIG=/config/$ARCH/$CONFIG_NAME.toml
export TARGET=$ARCH-unknown-redox

echo "Building Target $TARGET/$CONFIG_NAME"

cd / && export SIZE=$(redox_installer --filesystem-size -c $FILESYSTEM_CONFIG)
rm -rf /build/harddrive.img && truncate -s "${SIZE}m" /build/harddrive.img.partial && \
rm -rf /build/livedisk.iso && truncate -s "${SIZE}m" /build/livedisk.iso.partial && \
umask 002 && redox_installer --cookbook=cookbook -c $FILESYSTEM_CONFIG /build/harddrive.img.partial && \
umask 002 && redox_installer --cookbook=cookbook -c $FILESYSTEM_CONFIG --live /build/livedisk.iso.partial && \
mv /build/harddrive.img.partial /build/harddrive.img && 
mv /build/livedisk.iso.partial /build/livedisk.iso

